#! /bin/sh

### custom
ln -s ~/.dotfiles/custom/i3ws /usr/local/bin/

### rofi 
ln -s ~/.dotfiles/rofi/scripts/usbmount /usr/local/bin/
ln -s ~/.dotfiles/rofi/scripts/chres /usr/local/bin/
