#! /bin/sh

### vifm
ln -s ~/.dotfiles/.vifm/ ~/
ln -s ~/.dotfiles/vifm/ ~/.config/

### vim
ln -s ~/.dotfiles/vim/.vim/ ~/
ln -s ~/.dotfiles/vim/.vimrc ~/

### tmux
ln -s ~/.dotfiles/tmux/.tmux/ ~/
ln -s ~/.dotfiles/tmux/.tmux.conf ~/
ln -s ~/.dotfiles/tmux/tmux/ ~/.config/

### .Xresources
ln -s ~/.dotfiles/.Xresources ~/

### xinit
#ln -s ~/.dotfiles/.xinitrc ~/

### dunst
ln -s ~/.dotfiles/dunst/ ~/.config/

### Fonts
ln -s ~/.dotfiles/.fonts/ ~/

### xxkbrc
ln -s ~/.dotfiles/.xxkbrc ~/

### Zsh
ln -s ~/.dotfiles/zsh/.zshrc ~/
ln -s ~/.dotfiles/zsh/.zshrc.pre-oh-my-zsh ~/
ln -s ~/.dotfiles/zsh/.zsh ~/
ln -s ~/.dotfiles/zsh/.oh-my-zsh/ ~/

### i3
ln -s ~/.dotfiles/i3/.i3blocks.conf ~/
ln -s ~/.dotfiles/i3/.i3status.conf ~/
ln -s ~/.dotfiles/i3/i3/ ~/.config/

### Awesome
ln -s ~/.dotfiles/awesome/ ~/.config/

### Mc
ln -s ~/.dotfiles/mc/ ~/.config/

### Qtile
ln -s ~/.dotfiles/qtile/ ~/.config/

### Ranger
ln -s ~/.dotfiles/ranger/ ~/.config/

### Rofi
ln -s ~/.dotfiles/rofi/ ~/.config/

### Sublime text
ln -s ~/.dotfiles/sublime-text-3/ ~/.config/
ln -s ~/.dotfiles/zsh/.zsh

### Simple X Hotkey Daemon
ln -s ~/.dotfiles/sxhkd/ ~/.config/
